package MVCpattern;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class Controller {
    public enum menu1{
        START(),
        QUIT();
    }

    public static void start(){
        Map<String, String> menu = new LinkedHashMap<String, String>();
        menu.put("1", "1 - Start");
        menu.put("Q", "Q - Quit");
        View.display(menu);
        View.display(menu1.START);
        View.display(menu1.QUIT);
    }
}
